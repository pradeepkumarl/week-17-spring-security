package com.glearning.librarymanagement.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(IllegalArgumentException.class)
	public String handleInvalidBookId(IllegalArgumentException e) {
		return "invalid book id passed";
	}

}
