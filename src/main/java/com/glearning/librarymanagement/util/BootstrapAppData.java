package com.glearning.librarymanagement.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.glearning.librarymanagement.entity.Book;
import com.glearning.librarymanagement.entity.Role;
import com.glearning.librarymanagement.entity.User;
import com.glearning.librarymanagement.repository.BookRepository;
import com.glearning.librarymanagement.repository.UserRepository;

@Component
public class BootstrapAppData {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@EventListener(ApplicationReadyEvent.class)
	public void insertData(ApplicationReadyEvent event) {
		
		Book book = new Book();
		book.setName("Java-8");
		book.setAuthor("Hearbert Shield");
		book.setCategory("Programming");
		
		Book book2 = new Book();
		book2.setName("Nodejs");
		book2.setAuthor("Pinto Shield");
		book2.setCategory("Programming");

		Book book3 = new Book();
		book3.setName("Python");
		book3.setAuthor("Vin Shield");
		book3.setCategory("Programming");

		
		this.bookRepository.save(book);
		this.bookRepository.save(book2);
		this.bookRepository.save(book3);
		
	}
	
	@EventListener(ApplicationReadyEvent.class)
	public void insertUsers(ApplicationReadyEvent event) {
		
		User kiran = new User();
		kiran.setUsername("kiran");
		kiran.setPassword(this.passwordEncoder.encode("welcome"));
		
		User vinay = new User();
		vinay.setUsername("vinay");
		vinay.setPassword(this.passwordEncoder.encode("welcome"));
		
		
		Role userRole = new Role();
		userRole.setName("ROLE_USER");
		
		Role adminRole = new Role();
		adminRole.setName("ROLE_ADMIN");
		
		
		List<Role> roles = kiran.getRoles();
		roles.add(userRole);
		
		Role vinayUserRole = new Role();
		vinayUserRole.setName("ROLE_USER");
		
		List<Role> vinayRoles = vinay.getRoles();
		vinayRoles.add(adminRole);
		vinayRoles.add(vinayUserRole);
		
		
		this.userRepository.save(kiran);
		this.userRepository.save(vinay);
		
	}

}
