package com.glearning.librarymanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.glearning.librarymanagement.entity.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer>{
	
	Book findByAuthor(String name);

}
