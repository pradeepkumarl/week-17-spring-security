package com.glearning.librarymanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.glearning.librarymanagement.entity.Book;
import com.glearning.librarymanagement.service.BookService;

@RestController
@RequestMapping("/books")
public class BookRestController {

	@Autowired
	private BookService bookService;
	
	
	@GetMapping
	public List<Book> fectchAllBooks(){
		return this.bookService.findAll();
	}
	
	@GetMapping("/{id}")
	public Book fectchBookById(@PathVariable("id") int id){
		return this.bookService.findById(id);
	}
	
	@GetMapping("/author?{name}")
	public Book fectchBookByAuthor(@RequestParam(name="name", required = false, defaultValue = "ramesh") String name){
		return this.bookService.findBookByAuthorName(name);
	}
	
	@PostMapping
	public Book saveBook(@RequestBody Book book) {
		return this.bookService.save(book);
	}
	
	@DeleteMapping("/{id}")
	public void deleteBookById(@PathVariable("id") int id){
		this.bookService.deleteBookById(id);
	}
}
