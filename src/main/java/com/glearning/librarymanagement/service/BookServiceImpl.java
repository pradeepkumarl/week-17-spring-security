package com.glearning.librarymanagement.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glearning.librarymanagement.entity.Book;
import com.glearning.librarymanagement.repository.BookRepository;

//performing the business logic and will be saving the data to the database
@Service
public class BookServiceImpl implements BookService {
	
	@Autowired
	private BookRepository bookRepository;
	
	@Override
	public Book save(Book book) {
		Book savedBook = this.bookRepository.save(book);
		return savedBook;
	}

	@Override
	public List<Book> findAll() {
		List<Book> books = this.bookRepository.findAll();
		return books;
	}

	@Override
	public Book findById(int id) {
		Optional<Book> optionalBook = this.bookRepository.findById(id);
		if ( optionalBook.isPresent()) {
			return optionalBook.get();
		} else {
			throw new IllegalArgumentException("invalid book id is passed");
		}
	}

	@Override
	public void deleteBookById(int id) {
		this.bookRepository.deleteById(id);
	}

	@Override
	public Book findBookByAuthorName(String name) {
		return this.bookRepository.findByAuthor(name);
	}

}
