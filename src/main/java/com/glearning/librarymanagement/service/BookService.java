package com.glearning.librarymanagement.service;

import java.util.List;

import com.glearning.librarymanagement.entity.Book;

public interface BookService {
	
	public Book save(Book book);
	
	public List<Book> findAll();
	
	public Book findById(int id);
	
	public Book findBookByAuthorName(String name);
	
	public void deleteBookById(int id);

}
